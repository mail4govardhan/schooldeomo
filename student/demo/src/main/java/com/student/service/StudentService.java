package com.student.service;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.student.controller.StudentController;
import com.student.model.Student;

@Service
public class StudentService {

	private static final Logger logger = LoggerFactory.getLogger(StudentController.class);

	public static Map<Integer,Student> studentMap = new HashMap<>();
	
	String schoolUrl = "http://localhost:8081/SchoolService/school/";
	
	public Student saveStudentInfo(Student student) {
		logger.debug("schoolId"+student.getSchoolId());
		
		studentMap.put(student.getStudentId(), student);
		return student;
	}
	
	public Student getStudentInfo(int studentId) {
		logger.debug("Get Student:"+studentMap.get(studentId));
		return studentMap.get(studentId);
	}
}
