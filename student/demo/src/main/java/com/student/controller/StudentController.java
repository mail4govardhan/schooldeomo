package com.student.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.student.model.School;
import com.student.model.Student;
import com.student.service.StudentService;

@RestController
public class StudentController {

	private static final Logger logger = LoggerFactory.getLogger(StudentController.class);

	@Autowired
	private StudentService studentService;
	
	//we can use Eureka Discovery client instead rest template
	@Autowired
	private RestTemplate restTemplate;
	
	String schoolUrl = "http://localhost:8081/SchoolService/schools/";

	@RequestMapping(value = "/students", headers = "Accept=application/json", method = RequestMethod.POST)
	public String saveStudentInfo(@RequestBody Student student) {
		String output;
		logger.debug("StudentName:"+student.getStudentName());
		logger.debug("RestTemplate call School Service");
		School school = restTemplate.getForObject(schoolUrl+student.getSchoolId(), School.class);
		logger.debug(school!= null?school.getSchoolName():"school is not existed");
		if(school!= null && school.getSchoolName()!= null) {
			studentService.saveStudentInfo(student);
			output = "Student registered successfully to School: "+school.getSchoolName();
		}else {
			output = "school is not existed, please try with other SchoolId ";
		}

		return output;
	}
	
	@RequestMapping(value = "/student/{studentId}", method = RequestMethod.GET)
	public Student getStudentInfo(@PathVariable int studentId) {
		logger.debug("Get Student Details");
		return studentService.getStudentInfo(studentId);
	}
}
