package com.student.model;

public class Student {

	private int schoolId;
	
	private int studentId;
	
	private String studentName;
	
	public int getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(int schoolId) {
		this.schoolId = schoolId;
	}
	public int getStudentId() {
		return studentId;
	}
	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	@Override
	public String toString() {
		return "Student [schoolId=" + schoolId + ", studentId=" + studentId + ", studentName=" + studentName + "]";
	}
}
