package com.school.service;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.school.model.School;

@Service
public class SchoolService {

	private static final Logger logger = LoggerFactory.getLogger(SchoolService.class);

//	@Autowired
//	private SchoolRepository schoolRepository;
	
	public Map<Integer,School> schoolMap = new HashMap<>();

	public School saveSchoolInfo(School school) {
//		schoolRepository.save(school);
		logger.debug("School POJO:"+school.getSchoolId()+"--"+school.getSchoolName());

		schoolMap.put(school.getSchoolId(), school);

		return school;
	}
	
	public School getSchoolInfo(int schoolid) {
		logger.debug("Get School:"+schoolMap.get(schoolid));
//		return schoolRepository.findById(schoolid).get();
		return schoolMap.get(schoolid);
	}
}
