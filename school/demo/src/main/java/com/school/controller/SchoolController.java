package com.school.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.school.model.School;
import com.school.service.SchoolService;

@RestController
public class SchoolController {

	private static final Logger logger = LoggerFactory.getLogger(SchoolController.class);
 
	@Autowired
	private SchoolService schoolService;
	
	@RequestMapping(value = "/schools", headers = "Accept=application/json", method = RequestMethod.POST)
	public School saveStudentInfo(@RequestBody School school) {
		logger.info("saveStudentInfo start in SchoolController");
		logger.debug("School POJO:"+school.getSchoolId()+"--"+school.getSchoolName());
		return schoolService.saveSchoolInfo(school);

	}
	
	@GetMapping(value = "/schools/{schoolId}")
	public School getSchoolInfo(@PathVariable int schoolId) {
		logger.info("getSchoolInfo start in SchoolController");
		logger.debug("SchoolId:"+schoolId);
		return schoolService.getSchoolInfo(schoolId);
	}
}
