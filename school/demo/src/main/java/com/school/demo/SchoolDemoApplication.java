package com.school.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.school.*")
public class SchoolDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SchoolDemoApplication.class, args);
	}

}
