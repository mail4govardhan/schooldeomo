package com.school.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class School {

	@JsonProperty
	private int schoolId;
	
	@JsonProperty
	private String schoolName;
	
	public int getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(int schoolId) {
		this.schoolId = schoolId;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	
	@Override
	public String toString() {
		return "School [schoolId=" + schoolId + ", schoolName=" + schoolName + "]";
	}
}
